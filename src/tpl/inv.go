package tpl

import (
	"bytes"
	"github.com/sipin/gorazor/gorazor"
)

func Inv(invar *Inventory) string {
	var _buffer bytes.Buffer
	_buffer.WriteString(gorazor.HTMLEscape((invar.Count)))
	_buffer.WriteString(" items are made of ")
	_buffer.WriteString(gorazor.HTMLEscape((invar.Material)))

	return _buffer.String()
}
