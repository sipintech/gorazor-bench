package tpl

import (
	"bytes"
	"github.com/sipin/gorazor/gorazor"
)

func Test_func(input string) string {
	var _buffer bytes.Buffer
	for i := 0; i < 3; i++ {

		_buffer.WriteString("<p>Output ")
		_buffer.WriteString(gorazor.HTMLEscape(i))
		_buffer.WriteString(" : ")
		_buffer.WriteString(gorazor.HTMLEscape(input))
		_buffer.WriteString(" </p>")

	}

	return _buffer.String()
}
