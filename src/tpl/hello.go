package tpl

import (
	"bytes"

	"github.com/sipin/gorazor/gorazor"
)

func Hello(parameter string) string {
	var _buffer bytes.Buffer
	_buffer.WriteString("Hello, ")
	_buffer.WriteString(gorazor.HTMLEscape(parameter))
	_buffer.WriteString("!")

	return _buffer.String()
}
