package tpl

type Inventory struct {
	Material string
	Count    uint
}
