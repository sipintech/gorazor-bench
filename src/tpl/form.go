package tpl

import (
	"bytes"
	"github.com/sipin/gorazor/gorazor"
)

func Form(input string) string {
	var _buffer bytes.Buffer
	_buffer.WriteString("\n\n\n<!DOCTYPE html>\n<head>\n    <title>")
	_buffer.WriteString(gorazor.HTMLEscape(input))
	_buffer.WriteString("</title>\n<script>\nwindow.addEventListener(\"load\", function () {\nvar submitter = document.getElementById(\"submit\");\nsubmitter.addEventListener(\"click\", function() {\nvar form = document.getElementById(\"boop\");\nform.submit();\n});\n});\n</script>\n<style>\nbackground: #808080\n</style>\n</head>\n<body>\n<form id=\"boop\" method=\"POST\" action=\"/display\">\n<label for=\"tag\">Tag Injection:\n<input name=\"tag\" type=\"text\" value=\"\">\n</label><br>\n<label for=\"script\">Script Injection:\n<input name=\"script\" type=\"text\" value=\"\">\n</label><br>\n<label for=\"attr_double\">Double Quote Attribute Injection:\n<input name=\"attr_double\" type=\"text\" value=\"\">\n</label><br>\n<label for=\"attr_single\">Single Quote Attribute Injection:\n<input name=\"attr_single\" type=\"text\" value=\"\">\n</label><br>\n<label for=\"attr_onevent\">OnEvent Attribute Injection:\n<input name=\"attr_onevent\" type=\"text\" value=\"\">\n</label><br>\n<label for=\"attr_src\">Source Attribute Injection:\n<input name=\"attr_src\" type=\"text\" value=\"\">\n</label><br>\n<label for=\"style\">CSS/Style Injection:\n<input name=\"style\" type=\"text\" value=\"\">\n</label><br>\n<button id=\"submit\">Submit</button>\n</form>\n</body>\n</html>")

	return _buffer.String()
}
