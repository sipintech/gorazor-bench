package tpl

import (
	"bytes"
	"github.com/sipin/gorazor/gorazor"
)

func Base(input string) string {
	var _buffer bytes.Buffer
	_buffer.WriteString("\n\n<!DOCTYPE html>\n<html>\n<body>\n\n<header>\n<h1>Title of ")
	_buffer.WriteString(gorazor.HTMLEscape(input))
	_buffer.WriteString("</h1>\n<nav>\n<ul>\n<li><a href=\"hoge\">Hoge</a></li>\n<li><a href=\"piyo\">Piyo</a></li>\n</ul>\n</nav>\n</header>\n\n<article id=\"content\">\n\n</article>\n\n<footer>\n&copy; Copyright 2013 by golang-samples.\n</footer>\n</body>\n</html>")

	return _buffer.String()
}
