package bench

import (
	"bytes"
	"strings"
	"testing"
	"text/template"
	"tpl"
)

func TestTemplate(t *testing.T) {
	templ := template.Must(template.New("test").Parse("Hello, {{.}}!"))
	var buf1 bytes.Buffer
	templ.Execute(&buf1, "World")

	var buf2 bytes.Buffer
	buf2.WriteString(tpl.Hello("World"))

	if buf1.String() != buf2.String() {
		t.Error()
	}
}

func Benchmark_templat_1(b *testing.B) {
	templ := template.Must(template.New("test").Parse("Hello, {{.}}!"))
	for i := 0; i < b.N; i++ {
		var buf bytes.Buffer
		templ.Execute(&buf, "World")
	}
}

func Benchmark_gorazor_1(b *testing.B) {
	for i := 0; i < b.N; i++ {
		var buf bytes.Buffer
		buf.WriteString(tpl.Hello("World"))
	}
}

// --------------------------------------------------
func Benchmark_templat_2(b *testing.B) {
	tmpl := template.Must(template.New("test").Parse("{{.Count}} items are made of {{.Material}}"))
	for i := 0; i < b.N; i++ {
		var buf bytes.Buffer
		sweaters := tpl.Inventory{"wool", 17}
		tmpl.Execute(&buf, sweaters)
	}

}

func Benchmark_gorazor_2(b *testing.B) {
	for i := 0; i < b.N; i++ {
		var buf bytes.Buffer
		sweaters := &tpl.Inventory{"wool", 17}
		buf.WriteString(tpl.Inv(sweaters))
	}
}

// --------------------------------------------------
func Benchmark_templat_3(b *testing.B) {
	funcMap := template.FuncMap{
		"title": strings.Title,
	}
	const templateText = `
Input: {{printf "%q" .}}
Output 0: {{title .}}
Output 1: {{title . | printf "%q"}}
Output 2: {{printf "%q" . | title}}`

	tmpl, _ := template.New("titleTest").Funcs(funcMap).Parse(templateText)
	for i := 0; i < b.N; i++ {
		var buf bytes.Buffer
		tmpl.Execute(&buf, "the go programming language")
	}
}

func Benchmark_gorazor_3(b *testing.B) {
	for i := 0; i < b.N; i++ {
		var buf bytes.Buffer
		buf.WriteString(tpl.Test_func("the go programming language"))
	}
}

// --------------------------------------------------
func Benchmark_templat_4(b *testing.B) {
	tmpl, _ := template.ParseFiles("src/tpl/base.html")
	for i := 0; i < b.N; i++ {
		var buf bytes.Buffer
		tmpl.Execute(&buf, map[string]string{"Title": "My Title"})
	}
}

func Benchmark_gorazor_4(b *testing.B) {
	for i := 0; i < b.N; i++ {
		var buf bytes.Buffer
		buf.WriteString(tpl.Base("My Title"))
	}
}

// --------------------------------------------------
func Benchmark_templat_5(b *testing.B) {
	tmpl, err := template.ParseFiles("src/tpl/form.html")
	if err != nil {
		panic(err)
	}
	for i := 0; i < b.N; i++ {
		var buf bytes.Buffer
		tmpl.Execute(&buf, map[string]string{"Title": "My Title"})
	}
}

func Benchmark_gorazor_5(b *testing.B) {
	for i := 0; i < b.N; i++ {
		var buf bytes.Buffer
		buf.WriteString(tpl.Form("My Title"))
	}
}
